import pytest


def not_testable(func):
    @pytest.mark.skip(reason="not testable")
    def wrapper():
        func()

    return wrapper


def not_implemented(func):
    @pytest.mark.skip(reason="not implemented")
    def wrapper():
        func()

    return wrapper
