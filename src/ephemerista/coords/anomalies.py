import abc
from typing import Literal, TypeAlias

from hapsira.core.angles import E_to_M, E_to_nu, M_to_E, nu_to_E
from pydantic import Field

from ephemerista.angles import Angle

DISCRIMINATOR: Literal["anomaly_type"] = "anomaly_type"


class AbstractAnomaly(Angle, abc.ABC):
    degrees: float = Field(ge=-180, le=180, description="The value of the anomaly in degrees")

    @abc.abstractmethod
    def true_anomaly(self, eccentricity: float) -> float:
        """
        Returns the true anomaly of the osculating orbit in radians.

        Parameters
        ----------
        eccentricity : float
            The eccentricity of the orbit
        """
        pass

    @abc.abstractmethod
    def mean_anomaly(self, eccentricity: float) -> float:
        """
        Returns the mean anomaly of the osculating orbit in radians.

        Parameters
        ----------
        eccentricity : float
            The eccentricity of the orbit
        """
        pass


class TrueAnomaly(AbstractAnomaly):
    """Represents the true anomaly of an osculating orbit."""

    anomaly_type: Literal["true_anomaly"] = Field(
        default="true_anomaly", frozen=True, repr=False, alias="type", description="The type of the anomaly"
    )

    def true_anomaly(self, _eccentricity: float) -> float:
        return self._radians

    def mean_anomaly(self, eccentricity: float) -> float:
        return E_to_M(nu_to_E(self._radians, eccentricity), eccentricity)


class MeanAnomaly(AbstractAnomaly):
    """Represents the mean anomaly of an osculating orbit."""

    anomaly_type: Literal["mean_anomaly"] = Field(
        default="mean_anomaly", frozen=True, repr=False, alias="type", description="The type of the anomaly"
    )

    def true_anomaly(self, eccentricity: float) -> float:
        return E_to_nu(M_to_E(self._radians, eccentricity), eccentricity)

    def mean_anomaly(self, _eccentricity: float) -> float:
        return self._radians


Anomaly: TypeAlias = MeanAnomaly | TrueAnomaly
