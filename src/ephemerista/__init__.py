from pathlib import Path

import pydantic
import spiceypy as spice
from pydantic import ConfigDict
from pydantic.alias_generators import to_camel

data_dir = Path(__file__).parent.parent.parent.joinpath("resources")

spice.furnsh(
    [
        str(data_dir.joinpath("naif0012.tls")),
        str(data_dir.joinpath("pck00010.tpc")),
    ]
)


class BaseModel(pydantic.BaseModel):
    model_config = ConfigDict(alias_generator=to_camel)
