import warnings
from typing import List, Optional

from astropy import units as u
from pandas import DataFrame

COLUMNS = ("Name", "Lat", "Lon", "Altitude", "Operator")


class GroundStation:
    def __init__(self, name: str, lat: float, lon: float, alt: Optional[float] = None, operator: Optional[str] = None):
        """Initiliase a `GroundStation` object

        Args:
            name (str): name of the ground station
            lat (float): latitutde of the ground station
            lon (float): longitude of the ground station
            alt (str, optional): altitude of the ground station in `km`. Defaults to None.
            operator (str, optional): Name of the operator of the ground station. Example: "European Space Agency".
                Defaults to None.
        """
        self.name = name
        self.lat = lat
        self.lon = lon
        self.alt = alt * u.km if alt is not None else "Unknown"
        self.operator = operator


class GSNetwork:
    def __init__(self, name: Optional[str] = None):
        """A network of multiple ground stations

        Args:
            name (str, optional): Name of the ground station network. Example: "ESTRACK".
                Defaults to "My Ground Station Network".
        """
        self.name = name if name else "My Ground Station Network"
        self.ground_stations = {}

    def add_station(self, ground_station: GroundStation) -> None:
        """Adds a ground station to the network

        Args:
            ground_station (GroundStation): An instance of `GroundStation` class

        Returns:
            NoReturn
        """
        self.ground_stations[ground_station.name] = ground_station

    def get_station(self, station: str) -> GroundStation:
        """Get ground station by name (case sensitive)

        Args:
            station (str): name of a ground station. E.g. "Redu"

        Returns:
            GroundStation: Ground station instance from the network
        """
        station = self.ground_stations.get(station)
        if station is None:
            # todo decide how we do logging
            warning = (
                f"Could not find {station} in the {self.name} Ground Station network. Known ground stations:"
                f"{list(self.ground_stations.keys())}"
            )

            warnings.warn(
                warning,
                stacklevel=2,
            )
        return station

    def names(self) -> List[str]:
        """Returns a list of all Ground Station names in the network.

        Returns:
            List[str]: all Ground Station names in the network
        """
        return list(self.ground_stations.keys())

    def to_dataframe(self) -> DataFrame:
        """Returns a DataFrame of all ground stations in the network for easy data processing

        Returns:
            DataFrame: all ground stations in the network
        """
        return DataFrame(
            {
                "Name": [station.name for station in self.ground_stations.values()],
                "lat [deg]": [station.lat for station in self.ground_stations.values()],
                "lon [deg]": [station.lon for station in self.ground_stations.values()],
                "alt [km]": [
                    station.alt if isinstance(station.alt, str) else station.alt.value
                    for station in self.ground_stations.values()
                ],
                "Operator": [station.operator for station in self.ground_stations.values()],
            }
        )

    @classmethod
    def from_csv(cls, name: str, fp: str):
        """Initialise a `GSNetwork` object from a csv of ground stations.

        Args:
            name (str): a user-defined name of the network
            fp (str): filepath to the .csv file containing ground station info

        Raises:
            ValueError: If there are more than 4 elements in the csv, an error will be raised.
                See example for input data format.

        Returns:
            GSNetwork: `GSNetwork()` instance of the input ground stations.

        Example:

            Sample data format:
            '''
            # Name,Latitude,Longitude,Operator
            Kiruna,67.858428,20.966880,0,ESA
            Esrange Space Center,67.8833,21.1833,0,ESA
            '''

            Load data using
            ```
            from ephemerista.ground.groundstation import GSNetwork

            estrack = GSNetwork.from_csv(
                name="ESA Estrack Network", fp=os.path.join("./data/", "estrack.csv")
            )
            ````
        """
        network = cls(name=name)
        with open(fp) as file:
            for line in file:
                if line.startswith("#"):
                    continue
                data = line.strip().split(",")
                if len(data) > len(COLUMNS):
                    msg = f"Could not interpret line: {data} as a ground station name, lat, lon, alt, and operator."
                    raise ValueError(msg)
                # Store in the network by name, deliberately leaving case sensitive!
                network.ground_stations[data[0]] = GroundStation(
                    data[0], float(data[1]), float(data[2]), float(data[3]), data[4]
                )

        return network
