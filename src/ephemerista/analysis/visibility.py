from typing import Dict, List, Self

from astropy.time import Time
from pydantic import BaseModel

from ephemerista.scenarios import Scenario


class Window(BaseModel):
    start: str
    end: str

    @classmethod
    def from_epochs(cls, start: Time, end: Time) -> Self:
        return cls(start=str(start), end=str(end))


# def elevation(t: Time, gs: GroundStation, eph1: Ephem, eph2: Ephem) -> float:
#     r1 = np.concatenate(eph1.sample(t).xyz.value)
#     r2 = np.concatenate(eph2.sample(t).xyz.value)
#     lat = gs.latitude_radians()
#     lon = gs.longitude_radians()
#     min_ele = gs.minimum_elevation_radians()
#     fixed = gs.frame

#     r = r2 - r1
#     et = spice.utc2et(str(t))
#     mf = spice.pxform("J2000", fixed, et)
#     mt = bodyfixed_to_sez(lat, lon)
#     rt = mt @ mf @ r
#     return np.arcsin(rt[2] / np.linalg.norm(rt)) - min_ele


# def elevation_t(t: float | np.ndarray, t0: Time, gs: GroundStation, eph1: Ephem, eph2: Ephem) -> float | np.ndarray:
#     ep = t0 + TimeDelta(t * u.s)
#     if np.isscalar(t):
#         return elevation(
#             ep,
#             gs,
#             eph1,
#             eph2,
#         )
#     return np.array([elevation(epi, gs, eph1, eph2) for epi in ep])  # type: ignore


# def multi_root(f: Callable, bracket: Iterable[float], args: Iterable = (), n: int = 30) -> np.ndarray:
#     """Find all roots of f in `bracket`, given that resolution `n` covers the sign change.
#     Fine-grained root finding is performed with `scipy.optimize.root_scalar`.
#     Parameters
#     ----------
#     f: Callable
#         Function to be evaluated
#     bracket: Sequence of two floats
#         Specifies interval within which roots are searched.
#     args: Iterable, optional
#         Iterable passed to `f` for evaluation
#     n: int
#         Number of points sampled equidistantly from bracket to evaluate `f`.
#         Resolution has to be high enough to cover sign changes of all roots but not finer than that.
#         Actual roots are found using `scipy.optimize.root_scalar`.
#     Returns
#     -------
#     roots: np.ndarray
#         Array containing all unique roots that were found in `bracket`.
#     """
#     # Evaluate function in given bracket
#     x = np.linspace(*bracket, n)  # type: ignore
#     y = f(x, *args)

#     # Find where adjacent signs are not equal
#     sign_changes = np.where(np.sign(y[:-1]) != np.sign(y[1:]))[0]

#     # Find roots around sign changes
#     root_finders = (root_scalar(f=f, args=args, bracket=(x[s], x[s + 1])) for s in sign_changes)

#     roots = np.array([r.root if r.converged else np.nan for r in root_finders])

#     if np.any(np.isnan(roots)):
#         warnings.warn("Not all root finders converged for estimated brackets! Maybe increase resolution `n`.")
#         roots = roots[~np.isnan(roots)]

#     roots_unique = np.unique(roots)
#     if len(roots_unique) != len(roots):
#         warnings.warn(
#             "One root was found multiple times. "
#             "Try to increase or decrease resolution `n` to see if this warning disappears."
#         )

#     return roots_unique


def visibility(_scn: Scenario) -> Dict[str, Dict[str, List[Window]]]:
    # t0 = scn.start_date.to_astropy()
    # t1 = scn.end_date.to_astropy()
    # dt = float((t1 - t0).sec)  # type: ignore

    # ephems = scn.propagate()
    # assets = {asset.name: asset.model for asset in scn.assets}
    # keys = assets.keys()

    # for k1, k2 in product(keys, keys):
    #     if k1 == k2:
    #         continue

    #     a1 = assets[k1]

    #     if a1.asset_type != "groundstation":
    #         continue

    #     eph1 = ephems[k1]
    #     eph2 = ephems[k2]

    #     foo = multi_root(elevation_t, [0, dt], args=(t0, a1, eph1, eph2))

    return {"a": {"b": []}}
