from typing import List, Optional

import plotly.graph_objects as go
from astropy import units as u
from astropy.coordinates import GCRS, ITRS, CartesianDifferential, CartesianRepresentation, SphericalRepresentation
from astropy.time import Time
from hapsira.ephem import Ephem

from ephemerista.ground.groundstation import GroundStation, GSNetwork
from ephemerista.plot.config import (
    DEFAULT_GRID_COLOUR,
    DEFAULT_GROUND_STATION_COLOR,
    DEFAULT_GROUND_STATION_SIZE,
    DEFAULT_LAND_COLOR,
    DEFAULT_OCEAN_COLOR,
    DEFAULT_PLOTLY_THEME,
)


class GroundTrack:
    def __init__(
        self,
        ephem: Ephem,
        *,
        animate: bool = False,
        label: Optional[str] = None,
        title: Optional[str] = None,
        projection: Optional[str] = None,
    ) -> None:
        """This class is used to plot orbital Ground Tracks in various projections.

        Args:
            ephem (Ephem, optional): Must be passed in Earth-centric frames.
            label (str, optional): Label for the satellite object being plotted. Defaults to "My Satellite".
            title (str, optional): Title of the plot. Defaults to "{self.label} ".
            projection (str, optional): Earth map projection used by plotly's `Scattergeo` class.
                Defaults to "orthographic".

        Returns:
            NoReturn: _description_
        """
        self.fig = go.Figure(go.Scattergeo())

        # Update to relevant theme. Default: dark mode
        self.fig.update_layout(template=DEFAULT_PLOTLY_THEME)

        self.ephemeris = ephem
        self.projection = projection if projection else "orthographic"

        self.label = label if label else "My Satellite"
        self.title = title if title else f"{self.label} GroundTrack"
        self.ground_station_trace = []

        # Plot a naked Earth
        self._trace_earth()

        if animate:
            self.animate()
        else:
            # Plot the ground track
            self.fig.add_trace(self._trace_groundtrack(self.ephemeris))

    def animate(self, *, show=True):
        """Allows user to animate the existing ground track plot

        Args:
            show (bool, optional): Update the plot or pass show=True to display the plot immediately. Defaults to True.
        """
        # Compute the ground track once
        self.ground_track_trace = self._trace_groundtrack(self.ephemeris, name=f"{self.label}")

        # Compute & trace the spacecraft's position at every epoch
        self._animate_groundtrack()

        if show:
            self.fig.show()

    def _trace_earth(self) -> None:
        """Updates the `GroundTrack()` instances' `self.fig` (`:Scattergeo`) with a map projection from Earth

        Returns:
            NoReturn
        """
        self.fig.update_layout(
            title_text=self.title,
            # showlegend=False,
            geo={
                "showland": True,
                "showcountries": True,
                "showcoastlines": True,
                "showocean": True,
                "countrywidth": 0.5,
                "landcolor": DEFAULT_LAND_COLOR,
                "lakecolor": DEFAULT_OCEAN_COLOR,
                "oceancolor": DEFAULT_OCEAN_COLOR,
                "projection": {"type": self.projection},
                "lonaxis": {"showgrid": True, "gridcolor": DEFAULT_GRID_COLOUR, "gridwidth": 0.5},
                "lataxis": {"showgrid": True, "gridcolor": DEFAULT_GRID_COLOUR, "gridwidth": 0.5},
            },
        )

    def _convert_to_cartesian(self, ephemeris: Ephem) -> CartesianRepresentation:
        """Takes an ephemeris object and returns the cartesian (x,y,z) representation at each epoch.

        Args:
            ephemeris (Ephem): spacecraft ephemeris in J2000 Earth-centred frame

        Returns:
            CartesianRepresentation: cartesian (x,y,z) positions of the spacecraft at each epoch.
        """
        # todo migrate to a method of the new ephemeris class
        r, v = ephemeris.rv()
        cartesian = CartesianRepresentation(
            r,
            xyz_axis=-1,
            differentials=CartesianDifferential(v, xyz_axis=-1),
        )
        return cartesian

    def _j2000_to_itrs(self, cartesian: CartesianRepresentation, epochs: List[Time]) -> ITRS:
        """Converts cartesian representation to ITRS frame

        Args:
            cartesian (CartesianRepresentation): array of x,y,z with associated unit
            epochs (List[Time]): array of Epochs

        Returns:
            ITRS: Array of x,y,z at the input Epochs, in ITRS frame.
        """
        # todo migrate to a method of the new ephemeris class

        # Build GCRS and ITRS coordinates
        gcrs = GCRS(
            cartesian,
            obstime=epochs,
            representation_type=CartesianRepresentation,
        )
        itrs = gcrs.transform_to(ITRS(obstime=epochs))

        return itrs

    def _ephemeris_j2000_to_itrs(self, ephemeris: Ephem) -> ITRS:
        """Wrapper function. Takes an ephemeris object, puts it through transformations and returns an x,y,z cartesian
        representation of the ephemeris in ITRS frame.

        Args:
        ephemeris (Ephem): spacecraft ephemeris in J2000 Earth-centred frame

        Returns:
            ITRS: Array of x,y,z at the input Epochs, in ITRS frame.
        """
        # Pull out x,y,z representation from the ephemeris class
        cartesian = self._convert_to_cartesian(ephemeris)

        # Convert to ITRS
        itrs_cartesian = self._j2000_to_itrs(cartesian, ephemeris.epochs)

        # Convert to lat/lon
        return itrs_cartesian.represent_as(SphericalRepresentation)

    def _trace_groundtrack(self, ephemeris: Ephem, name=None) -> go.Scattergeo:
        """Takes a satellite's ephemeris, performs the frame transformation to latitutude and longitude, then returns a
        `Scattergeo` trace.

        Args:
            ephemeris (Ephem): satellite ephemeris in Earth centred J2000 frame.

        Returns:
            go.Scattergeo: trace of the ground track latitutude and longitude.
        """
        # Convert ephemeris to lat/lon
        itrs = self._ephemeris_j2000_to_itrs(ephemeris)

        # Return ground track
        return go.Scattergeo(
            lat=itrs.lat.to(u.deg),
            lon=itrs.lon.to(u.deg),
            hovertext=ephemeris.epochs.value,
            mode="lines",
            name=name if name else f"{self.label} Groundtrack",
            line={"color": "rgb(253,245,230)"},
        )

    def _trace_spacecraft_position(self, r, epoch) -> go.Scattergeo:
        """Takes cartesian j2000 coordinates and an epoch, and returns a Scattergeo trace of the lat/lon of the
        spacecraft.

        Args:
            r (): j2000 cartesian position vector, e.g. [1312.12, -3412.45, 6370.23,]
            epoch (): Epoch in UTC

        Returns:
            go.Scattergeo: Scattergeo trace
        """
        itrs = GCRS(
            r,
            obstime=epoch,
            representation_type=CartesianRepresentation,
        ).transform_to(ITRS(obstime=epoch))

        itrs = itrs.represent_as(SphericalRepresentation)

        marker = {
            "size": 10,
            "color": "black",
            "line": {"width": 2, "color": "black"},
            "symbol": "circle",
            "opacity": 0.7,
        }

        return go.Scattergeo(
            lat=[itrs.lat.to(u.deg).value],
            lon=[itrs.lon.to(u.deg).value],
            mode="markers",
            marker=marker,
            name=f"{self.label} Position",
            hovertext=epoch.iso,
        )

    def _animate_groundtrack(self) -> None:
        """Adds  slider bar to the GroundTrack plot allowing the user to change the displayed epoch and update
        spacecraft position.

        Returns:
            NoReturn
        """
        # Set all traces to False by default
        visible_states = [[False] * len(self.ephemeris.epochs) for _ in range(len(self.ephemeris.epochs))]
        spacecraft_traces = []

        # Iterate each epoch in the ephemeris
        for idx, _epoch in enumerate(self.ephemeris.epochs):
            # todo there should be a way to add these traces only once and have them remain visible at all timesteps
            # Add the ground track to every timestep, so it remains visible in the plot
            self.fig.add_trace(self.ground_track_trace)

            # Compute spacecraft position at this epoch
            r = self.ephemeris.rv()[0][idx]
            t = self.ephemeris.epochs[idx]
            spacecraft_traces.append(self._trace_spacecraft_position(r, t))

            # Set this trace' index as visible
            visible_states[idx][idx] = True

        # For some reason, appending traces within the enumeraiton causes synch issues, so we collect and add afterward.
        for trace in spacecraft_traces:
            self.fig.add_trace(trace)

        # Define the steps for the slide bar
        slider_steps = [
            {
                "method": "update",
                "label": str(epoch.iso),
                "args": [
                    {"visible": visible_states[idx]},
                    {"title": f"{self.title} - {epoch.iso}"},
                ],
            }
            for idx, epoch in enumerate(self.ephemeris.epochs)
        ]

        # Add the side bar to the plot
        self.fig.update_layout(
            sliders=[
                {
                    "active": len(self.ephemeris.epochs) / 2,  # Set the slider in the middle
                    "currentvalue": {"prefix": "Epoch: "},
                    "steps": slider_steps,
                }
            ]
        )

    def plot_groundstation(
        self,
        gs: GroundStation,
        color: str = DEFAULT_GROUND_STATION_COLOR,
        size: float = DEFAULT_GROUND_STATION_SIZE,
        name: Optional[str] = None,
        hovertext: Optional[str] = None,
    ) -> None:
        """Takes a GroundStation object and plots it on the GroundTrack plot instance.
        Args:
            gs (GroundStation): `ephemerista.ground.ground_station.GroundStation` object
            color (str, optional): Colour of the ground station marker. Defaults to DEFAULT_GROUND_STATION_COLOR.
            size (float, optional): Size of the ground station marker. Defaults to DEFAULT_GROUND_STATION_SIZE.
            name (str, optional): Name of the ground station. Defaults to `GroundStation.name`.
            hovertext (str, optional): Desired hover text info for the ground station. Defaults to `GroundStation.name`.

        Returns:
            NoReturn
        """
        # Define the marker
        marker = {
            "size": size,
            "color": color,
            "line": {"width": 2, "color": DEFAULT_GROUND_STATION_COLOR},
            "symbol": "circle",
            "opacity": 0.7,
        }

        trace = go.Scattergeo(
            name=gs.name if not name else name,
            lat=[gs.lat],
            lon=[gs.lon],
            mode="markers",
            marker=marker,
            hovertext=f"{gs.name}" if not hovertext else hovertext,
        )

        self.ground_station_trace.append(trace)
        self.fig.add_trace(trace)

    def plot_gsnetwork(
        self,
        network: GSNetwork,
        color: str = DEFAULT_GROUND_STATION_COLOR,
        size: float = DEFAULT_GROUND_STATION_SIZE,
    ):
        """Takes a `GSNetwork` object and plots all associated ground stations on the GroundTrack plot instance.
        Calls `self.fig.show()` at the end of the function to display the new additions.

        Args:
            network (GSNetwork): `ephemerista.ground.ground_station.GSNetwork` object
            color (str, optional): Colour of the ground station marker. Defaults to DEFAULT_GROUND_STATION_COLOR.
            size (float, optional): Size of the ground station marker. Defaults to DEFAULT_GROUND_STATION_SIZE.

        Returns:
            NoReturn
        """
        for station in network.ground_stations.values():
            self.plot_groundstation(station, color, size)

        return self.fig.show()

    def update_projection(self, p: str) -> None:
        """Allows user to update the projection method of the plotly geosca.

        Args:
            p (str): either "orthographic" or "equirectangular"

        Returns:
            NoReturn
        """
        self.fig.update_geos(projection_type=p)
        self.projection = p

    def plot(self):
        """Allows user to intuitively show the plot.

        Returns:
            NoReturn
        """
        return self.fig.show()
