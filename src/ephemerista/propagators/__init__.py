from typing import TypeAlias, Union

from ephemerista.propagators.hapsira import HapsiraPropagator
from ephemerista.propagators.spice import SpiceKernelPropagator

Propagator: TypeAlias = Union[HapsiraPropagator, SpiceKernelPropagator]
