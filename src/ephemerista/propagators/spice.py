from pathlib import Path
from typing import List, Literal

import numpy as np
import spiceypy as spice
from astropy.coordinates import CartesianDifferential, CartesianRepresentation
from astropy.time import Time
from hapsira.ephem import Ephem
from hapsira.frames.enums import Planes
from pydantic import BaseModel, Field


class SpiceKernelPropagator(BaseModel):
    propagator_type: Literal["spice"] = Field(default="spice", alias="type")
    naif_id: str = Field(alias="id")

    @staticmethod
    def load_kernel(path: Path) -> None:
        spice.furnsh(str(path))

    def propagate(self, epochs: List[Time]):
        ets = np.array([spice.utc2et(ep.to_string()) for ep in epochs])
        states, _ = spice.spkezr(self.naif_id, ets, "J2000", "NONE", "399")
        x = np.array([state[0] for state in states])
        y = np.array([state[1] for state in states])
        z = np.array([state[2] for state in states])
        vx = np.array([state[3] for state in states])
        vy = np.array([state[4] for state in states])
        vz = np.array([state[5] for state in states])
        c_vel = CartesianDifferential(d_x=vx, d_y=vy, d_z=vz, unit="km/s")
        c_pos = CartesianRepresentation(x, y, z, unit="km", differentials={"s": c_vel})
        times = Time(np.array([t.jd for t in epochs]), format="jd")
        return Ephem(c_pos, times, Planes.EARTH_EQUATOR)
