from typing import List, Literal, TypeAlias, Union

from astropy.time import Time
from hapsira.ephem import Ephem
from hapsira.twobody import propagation as prop
from pydantic import BaseModel, Field

from ephemerista.coords.twobody import Cartesian, Keplerian

Method: TypeAlias = Union[
    Literal["cowell"],
    Literal["danby"],
    Literal["farnocchia"],
    Literal["gooding"],
    Literal["markley"],
    Literal["mikkola"],
    Literal["pimienta"],
    Literal["recseries"],
    Literal["vallado"],
]


class HapsiraPropagator(BaseModel):
    propagator_type: Literal["hapsira"] = Field(default="hapsira", alias="type")
    method: Method = Field(default="farnocchia")
    initial_state: Cartesian | Keplerian = Field(discriminator="state_type", alias="initialState")

    def propagator(self):
        match self.method:
            case "cowell":
                return prop.CowellPropagator()
            case "danby":
                return prop.DanbyPropagator()
            case "farnocchia":
                return prop.FarnocchiaPropagator()
            case "gooding":
                return prop.GoodingPropagator()
            case "markley":
                return prop.MarkleyPropagator()
            case "mikkola":
                return prop.MikkolaPropagator()
            case "pimienta":
                return prop.PimientaPropagator()
            case "recseries":
                return prop.RecseriesPropagator()
            case "vallado":
                return prop.ValladoPropagator()
            case _:
                return prop.FarnocchiaPropagator()

    def propagate(self, epochs: List[Time]) -> Ephem:
        orb = self.initial_state.to_orbit()
        return Ephem.from_orbit(orb, epochs)
