MAX_INCLINATION = 180
MAX_AOP = 360


class KeplerianValidator:
    @staticmethod
    def is_physical(a=None, e=None, i=None, aop=None):
        """Basic physicallity checks. All args are optional.

        Args:
            a (_type_, optional): _description_. Defaults to None.
            e (_type_, optional): _description_. Defaults to None.
            i (_type_, optional): _description_. Defaults to None.
            aop (_type_, optional): _description_. Defaults to None.

        Returns:
            _type_: _description_
        """
        # Semi-major axis (a) should be greater than 0
        if a is not None and a <= 0:
            return False, f"Semi-major axis should be positive. Received: {a}"

        # Eccentricity (e) should be between 0 and 1
        if e is not None and not 0 <= e < 1:
            return False, f"Eccentricity is unphysical. Received: {e}"

        # Inclination (i) should be between 0 and 180 degrees
        if i is not None and not 0 <= i <= MAX_INCLINATION:
            return False, f"Inclination should be expressed in degrees, between 0 and 180. Received: {i}"

        earth_radius = 6371  # todo migrate to constants
        if a is not None and e is not None and a * (1 - e) < earth_radius:
            return False, "Perigee crosses Earth Radius!"

        if aop and not 0 <= aop <= MAX_AOP:
            return False, "Argument of Perigee should be expressed in degrees, between 0 and 360."

        return True, "Gucci"
