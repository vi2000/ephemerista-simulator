from hapsira.twobody import Orbit


class Satellite:
    def __init__(self, name, orbit: Orbit, cd=2.2, mass=150):
        self.name = name
        self._orbit = orbit
        self.cd = cd  # drag coeff
        self.mass = mass  # * u.kg

    @property
    def orbit(self):
        return self._orbit
