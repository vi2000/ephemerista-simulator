import math
from typing import Literal, TypeAlias

import numpy as np

Frame: TypeAlias = (
    Literal["ICRF"]
    | Literal["IAU_MERCURY"]
    | Literal["IAU_VENUS"]
    | Literal["IAU_EARTH"]
    | Literal["IAU_MARS"]
    | Literal["IAU_JUPITER"]
    | Literal["IAU_SATURN"]
    | Literal["IAU_URANUS"]
    | Literal["IAU_NEPTUNE"]
    | Literal["IAU_PLUTO"]
)


def flattening(r_eq: float, r_p: float) -> float:
    return (r_eq - r_p) / r_eq


def lla_to_ecef(lat: float, lon: float, alt: float, r_e: float, f: float) -> np.ndarray:
    e = math.sqrt(2 * f - f**2)
    c = r_e / math.sqrt(1 - e**2 * math.sin(lat) ** 2)
    s = c * (1 - e**2)
    r_delta = (c + alt) * math.cos(lat)
    r_k = (s + alt) * math.sin(lat)
    return np.array([r_delta * math.cos(lon), r_delta * math.sin(lon), r_k])
