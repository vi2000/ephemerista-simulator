"""
This module contains the :py:class:`Time` class which is Ephemerista's representation for points in time (epochs).
"""

import datetime
from typing import Literal, Self, TypeAlias, cast

import astropy.time
import lox_space as lox
from pydantic import Field, PrivateAttr

from ephemerista import BaseModel

JD_J2000: float = 2451545.0

Scale: TypeAlias = (
    Literal["TT"] | Literal["TAI"] | Literal["TDB"] | Literal["TCG"] | Literal["TCB"] | Literal["UT1"] | Literal["UTC"]
)


class ISOTimestamp(BaseModel):
    time_type: Literal["iso"] = Field(default="iso", alias="type", repr=False, description="The type of the timestamp")
    value: str


class JulianDateTimestamp(BaseModel):
    time_type: Literal["jd"] = Field(default="jd", alias="type", repr=False, description="The type of the timestamp")
    value: float


class Time(BaseModel):
    scale: Scale = Field(default="UTC")
    timestamp: ISOTimestamp | JulianDateTimestamp = Field(discriminator="time_type")
    _time: lox.Time = PrivateAttr()
    _astropy: astropy.time.Time = PrivateAttr()

    def __init__(self, time: lox.Time | None = None, **data):
        super().__init__(**data)

        # TODO: Remove once LOX supports parsing timestamps
        if isinstance(self.timestamp, ISOTimestamp):
            self._astropy = astropy.time.Time(self.timestamp.value, format="isot", scale=self.scale.lower())
        else:
            self._astropy = astropy.time.Time(self.timestamp.value, format="jd", scale=self.scale.lower())
        dt = cast(datetime.datetime, self._astropy.datetime)
        ms = dt.microsecond // 1000
        mus = dt.microsecond - ms * 1000

        if not time:
            self._time = lox.Time(
                self.scale,
                dt.year,
                dt.month,
                dt.day,
                dt.hour,
                dt.minute,
                dt.second,
                ms,
                mus,
            )
        else:
            self._time = time

    @classmethod
    def _from_lox(cls, time: lox.Time) -> Self:
        scale = time.scale()
        jd = time.days_since_j2000() + JD_J2000
        timestamp = JulianDateTimestamp(value=jd)
        return cls(scale=scale, timestamp=timestamp, time=time)

    @classmethod
    def from_iso(cls, scale: Scale, iso: str) -> Self:
        """
        Instantiate a Time object from an ISO8601-formatted string in the given time scale.

        Parameters
        ----------
        scale : Scale
            The time scale of the timestamp
        iso : str
            An ISO8601-formatted timestamp
        """
        return cls(scale=scale, timestamp=ISOTimestamp(value=iso))

    @classmethod
    def from_julian_date(cls, scale: Scale, jd: float) -> Self:
        """
        Instantiate a Time object from a Julian Date in the given time scale.

        Parameters
        ----------
        scale : Scale
            The time scale of the timestamp
        jd : float
            The Julian Date of the timestamp
        """
        return cls(scale=scale, timestamp=JulianDateTimestamp(value=jd))

    @classmethod
    def from_j2000(cls, scale: Scale, j2000: float) -> Self:
        """
        Instantiate a Time object from a Julian Date based on the J2000 epoch in the given time scale.

        Parameters
        ----------
        scale : Scale
            The time scale of the timestamp
        jd : float
            The Julian Date of the timestamp
        """
        return cls.from_julian_date(scale, j2000 + JD_J2000)

    @property
    def astropy(self) -> astropy.time.Time:
        return self._astropy

    @property
    def datetime(self) -> datetime.datetime:
        return cast(datetime.datetime, self.astropy.datetime)

    @property
    def year(self) -> int:
        return self.datetime.year

    @property
    def month(self) -> int:
        return self.datetime.month

    @property
    def day(self) -> int:
        return self.datetime.day

    @property
    def hour(self) -> int:
        return self.datetime.hour

    @property
    def minute(self) -> int:
        return self.datetime.minute

    @property
    def second(self) -> int:
        return self.datetime.second

    @property
    def millisecond(self) -> int:
        return round(self.datetime.microsecond * 1e-3)

    @property
    def julian_date(self) -> float:
        t = astropy.time.Time(self.astropy, format="jd")
        return cast(float, t.value)
