from pydantic import BaseModel


class Spacecraft(BaseModel):
    pass
    # asset_type: Literal["spacecraft"] = Field(default="spacecraft", alias="type")
    # propagator: Propagator = Field(discriminator="propagator_type")

    # def propagate(self, times: List[astropy.time.Time]) -> Ephem:
    #     return self.propagator.propagate(times)


class GroundStation(BaseModel):
    pass
    # asset_type: Literal["groundstation"] = Field(default="groundstation", alias="type")
    # body: CentralBody = Field(default="Earth")
    # frame: str = Field(default="IAU_EARTH")
    # latitude: float = Field(ge=-90, le=90)
    # longitude: float = Field(ge=-180, le=180)
    # altitude: float = Field(default=0)
    # minimum_elevation: float = Field(ge=0.0, default=0.0, alias="minElevation")

    # def latitude_radians(self) -> float:
    #     return math.radians(self.latitude)

    # def longitude_radians(self) -> float:
    #     return math.radians(self.longitude)

    # def minimum_elevation_radians(self) -> float:
    #     return math.radians(self.minimum_elevation)

    # def propagate(self, epochs: List[astropy.time.Time]) -> Ephem:
    #     states = [
    #         lla_to_icrf(self.body, self.frame, ep, self.latitude_radians(), self.longitude_radians(), self.altitude)
    #         for ep in epochs
    #     ]
    #     x = np.array([state[0] for state in states])
    #     y = np.array([state[1] for state in states])
    #     z = np.array([state[2] for state in states])
    #     vx = np.array([state[3] for state in states])
    #     vy = np.array([state[4] for state in states])
    #     vz = np.array([state[5] for state in states])
    #     c_vel = CartesianDifferential(d_x=vx, d_y=vy, d_z=vz, unit="km/s")
    #     c_pos = CartesianRepresentation(x, y, z, unit="km", differentials={"s": c_vel})
    #     times = astropy.time.Time(np.array([t.jd for t in epochs]), format="jd")
    #     return Ephem(c_pos, times, Planes.EARTH_EQUATOR)


class Asset(BaseModel):
    pass
    # asset_id: UUID4 = Field(alias="id")
    # name: str
    # model: Spacecraft | GroundStation = Field(discriminator="asset_type")


class Scenario(BaseModel):
    pass
    # scenario_id: UUID4 = Field(alias="id")
    # name: str
    # start_date: Time = Field(alias="startDate")
    # end_date: Time = Field(alias="endDate")
    # time_step: float = Field(alias="timeStep")
    # assets: List[Asset]

    # def propagate(self):
    #     start_epoch = self.start_date.astropy
    #     end_epoch = self.end_date.astropy
    #     interval = end_epoch - start_epoch
    #     n_steps = (interval.sec * u.s) / (self.time_step * u.s)
    #     epochs = [*list(start_epoch + TimeDelta(self.time_step * u.s) * np.arange(n_steps)), end_epoch]
    #     return {asset.name: asset.model.propagate(epochs) for asset in self.assets}
