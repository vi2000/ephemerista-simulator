from pydantic import BaseModel, Field

from ephemerista.comms.antennas import Antenna
from ephemerista.comms.utils import to_db


class Transmitter(BaseModel):
    antenna: Antenna
    power: float = Field(gt=0.0)
    frequency: float = Field(gt=0.0)
    line_loss: float = Field(ge=0.0)

    def equivalent_isotropic_radiated_power(self, angle: float) -> float:
        return self.antenna.gain(self.frequency, angle) + to_db(self.power) - self.line_loss
