from pydantic import BaseModel, Field

from ephemerista.comms.antennas import Antenna
from ephemerista.comms.utils import to_db

ROOM_TEMP = 290  # K


def noise_temperature(noise_figure: float) -> float:
    return ROOM_TEMP * (10 ** (noise_figure / 10) - 1)


def system_temperature(t_antenna: float, loss: float, noise_figure: float) -> float:
    return t_antenna * 10 ** (-loss / 10) + ROOM_TEMP * (1 - 10 ** (-loss / 10)) + noise_temperature(noise_figure)


class Receiver(BaseModel):
    antenna: Antenna
    t_antenna: float = Field(gt=0.0, default=265)
    lna_gain: float = Field(gt=0.0)
    lna_noise_figure: float = Field(ge=0.0)
    noise_figure: float = Field(ge=0.0)
    loss: float = Field(ge=0.0)

    def gain_to_noise_temperature(self, frequency: float, angle: float) -> float:
        t_sys = system_temperature(self.t_antenna, self.loss, self.noise_figure)
        return self.antenna.gain(frequency, angle) - to_db(t_sys) + self.lna_gain - self.lna_noise_figure - self.loss
