import itur.models.itu837
import itur.utils
import numpy as np

"""

The calculations are performed according to
"Satellite Communication Systems: Systems, Techniques and Technologies"
by G. Maral and M. Bousquet

---

The aT_mospheric attenuation is modeled according to JPL/DSN aT_mospheric
model by Dr. Stephen D. Slobin
(ref: https://descanso.jpl.nasa.gov/monograph/series10/06_Reid_chapt6.pdf)

"""


class LinkBudget:
    def __init__(self):
        ## Input
        self.f = 35.10  # radio self.f (GHz)
        self.transmission = "UPLINK"
        self.etaSat = 0.5  # efficiency of sat antenna (0<η<1)
        self.D_sat = 0.3  # Satellite Antenna diameter (m)
        self.etaEarth = 0.45  # efficiency of earth antenna (0<η<1)
        self.D_earth = 0.98  # Earth Antenna diameter (m)
        self.depolEarth = 0.01  # earth depolarization angle (degrees)
        self.depolSat = 0.1  # misalignment of angles of sat
        self.lat_GS = 42.3601  # earth station latitude (degrees)
        self.lon_GS = -71.0942  # earth station longitude relative to satellite (degrees)
        self.lat_sat = 0  # satellite latitude relative to satellite (degrees)
        self.lon_sat = -77  # satellite longitude relative to satellite (degrees)
        self.h_sat = 800  # height of the satellite in km
        self.LFEarthX = 0.5  # earth station equipment loss (dB)
        self.LFSatX = 1  # satellite station equipment loss (dB)
        self.polangel = 1  # receiver polarization mismatch angle (degrees)
        self.PTX = 100  # transmitted power (W)
        self.T_A = 290  # Antenna temperature in K
        self.BIR = 50000  # Bit rate input (kbps)
        self.roll_off = 1.5
        self.MOD = 4  # QPSK=4, 8PSK=8
        self.FEC = 1 / 2  # QPSK, Forward Error Correction

        ## Constants
        self.c = 299792458  # RF waves velocity (m/s)
        self.R0 = 35786e3  # GEO orbit distance
        self.earth_radius = 6378.136  # Earth radius in km
        self.kdB = -228.6  # Boltzmann constant

        # Calculated values
        self.elevation = itur.utils.elevation_angle(self.h_sat, self.lat_sat, self.lon_sat, self.lat_GS, self.lon_GS)
        self.p = itur.models.itu837.rainfall_probability(self.lat_GS, self.lon_GS)
        self.rain_rate = itur.models.itu837.rainfall_rate(self.lat_GS, self.lon_GS, float(self.p))  # mm/hr
        r = self.h_sat + self.earth_radius
        self.slant_range = self.earth_radius * (
            (((r**2 / self.earth_radius**2) - (np.cos(self.elevation / 57.2958)) ** 2) ** 0.5)
            - np.sin(self.elevation / 57.2958)
        )

        ## Functions

        self.calculate_total_loss()
        self.calculate_antenna_gain()
        self.calculate_power()
        self.c_to_n0_ratio()
        # self.displayInputValues()
        # self.displayOutputValues()

    def c_to_n0_ratio(self):
        self.CtoN0 = 10 * np.log10(self.PTX) + self.G_T_max + self.G_R_max - self.L - 10 * np.log10(self.T_A) - self.kdB
        self.bandwidth = (self.BIR * 1000 * (1 + self.roll_off)) / (np.log2(self.MOD) * self.FEC * 1000)
        self.spectral_efficiency = self.BIR / self.bandwidth
        self.CtoN = self.CtoN0 - np.log10(self.bandwidth * 1000)  # the carrier power over the noise power

    # def displayInputValues(self):
    #     print("---------------------------------- Input ----------------------------------")
    #     print(f"Frequency                                                        {self.f:.2f} Hz")
    #     print(f"Transmission type                                                {self.transmission} ")

    #     print(f"Efficiency of the satellite antenna                              {self.etaSat:.2f} ")
    #     print(f"Satellite antenna diameter                                       {self.D_sat:.2f} m")
    #     print(f"Efficiency of the GS antenna                                     {self.etaEarth:.2f} ")
    #     print(f"GS antenna diameter                                              {self.D_earth:.2f} m")
    #     print(f"Receiver antenna temperature                                     {self.T_A:.2f} K")
    #     print(f"Transmitted power                                                {self.T_A:.2f} W")

    #     print(f"GS latitude                                                      {self.lat_GS:.2f} deg")
    #     print(f"GS longitude                                                     {self.lon_GS:.2f} deg")
    #     print(f"Satellite latitude                                               {self.lat_sat:.2f} deg")
    #     print(f"Satellite longitude                                              {self.lon_sat:.2f} deg")
    #     print(f"Satellite height                                                 {self.h_sat:.2f} km")

    #     print(f"GS equipment loss                                                {self.LFEarthX:.2f} dB")
    #     print(f"Satellite equipment loss                                         {self.LFSatX:.2f} dB")
    #     print(f"Earth depolarization angle                                       {self.depolEarth:.2f} deg")
    #     print(f"Satellite depolarization angle                                   {self.depolSat:.2f} deg")
    #     print(f"Receiver polarization mismatch angle                             {self.polangel:.2f} deg")
    #     print(f"Bit rate input                                                   {self.BIR:.2f} kbps")
    #     print(f"Roll off                                                         {self.roll_off:.2f}")
    #     print(f"MOD (QPSK=4, 8PSK=8)                                             {self.MOD:.2f}")
    #     print(f"Forward Error Correction                                         {self.FEC:.2f}")

    # def displayOutputValues(self):
    #     print("--------------------------------- Output ---------------------------------")
    #     print(f"Carrier power-to-noise power spectral density ratio (C/N0)       {self.CtoN0:.2f} dB*Hz")
    #     print(f"Bandwidth                                                        {self.bandwidth:.2f} kHz")
    #     print(f"EIRP                                                             {self.EIRP:.2f} dB")
    #     print(f"Free Space Attenuation                                           {self.L_fs:.2f} dB")
    #     print(f"Receiving antenna depointing loss                                {self.LR:.2f} dB")
    #     print(f"Transmitting antenna depointing loss                             {self.LT:.2f} dB")
    #     print(f"Polarisation mismatch loss                                       {self.L_POL:.2f} dB")
    #     print(f"Atmospheric Attenuation                                          {self.LA:.2f} dB")

    def calculate_power(self):
        self.EIRP = 10 * np.log10(self.PTX) + self.G_T_max  # Equivalent isotropic radiated power (dB)
        pwr_dbw = self.EIRP + self.G_R_max - self.L  # received power in dBW
        self.PRX = 10 ** (pwr_dbw / 10)  # received power in W

    def calculate_antenna_gain(self):
        lambda_m = self.c / (self.f * 1e9)  # wavelength (m)
        g_earth_max = 10 * np.log10(self.etaEarth * (np.pi * self.D_earth / lambda_m) ** 2)
        g_sat_max = 10 * np.log10(self.etaSat * (np.pi * self.D_sat / lambda_m) ** 2)
        if self.transmission == "UPLINK":
            self.G_T_max = g_earth_max
            self.G_R_max = g_sat_max
        elif self.transmission == "DOWNLINK":
            self.G_T_max = g_sat_max
            self.G_R_max = g_earth_max
        else:
            msg = "Transmission input must be either Uplink or Downlink"
            raise ValueError(msg)

    def calculate_free_space_attenuation(self):
        lambda_m = self.c / (self.f * 1e9)  # wavelength (m)
        self.L_fs = 20 * np.log10(4 * np.pi * self.slant_range * 1000 / lambda_m)  # Free space loss in dB

    def calculate_antenna_depointing_loss(self):
        lambda_m = self.c / (self.f * 1e9)  # wavelength (m)
        theta_earth_3db = 70 * lambda_m / self.D_earth  # Three dB angle of transmitter / Antenna Beamwidth
        theta_sat_3db = 70 * lambda_m / self.D_sat  # Three dB angle of receiver / Antenna Beamwidth
        # ~~ p.172 eq. 5.18~~ #
        l_earth = 12 * (self.depolEarth / theta_earth_3db) ** 2  # Transmitting antenna depointing loss in dB
        l_sat = 12 * (self.depolSat / theta_sat_3db) ** 2  # Receiving antenna depointing loss in dB
        if self.transmission == "UPLINK":
            self.LT = l_earth  # Earth station equipment loss (dB)
            self.LR = l_sat  # Satellite equipment loss (dB)
        elif self.transmission == "DOWNLINK":
            self.LT = l_sat
            self.LR = l_earth
        else:
            msg = "Transmission input must be either Uplink or Downlink"
            raise ValueError(msg)

    def calculate_equipment_loss(self):
        if self.transmission == "UPLINK":
            self.LFTX = self.LFEarthX
            self.LFRX = self.LFSatX
        elif self.transmission == "DOWNLINK":
            self.LFTX = self.LFSatX
            self.LFRX = self.LFEarthX
        else:
            msg = "Transmission input must be either Uplink or Downlink"
            raise ValueError(msg)

    def calculate_polarisation_mismatch_loss(self):
        """
        Where a circularly polarised antenna receives a linearly
        polarised wave, or a linearly polarised antenna receives
        a circularly polarised wave, LPOL has a value of 3 dB.
        """
        # ~~ p.176 ~~ #
        self.L_POL = abs(20 * np.log10(np.cos(self.polangel * np.pi / 180)))  # Polarisation mismatch loss

    def calculate_atmospheric_attenuation(self):
        a_g, a_c, a_r, a_s, a_t = itur.atmospheric_attenuation_slant_path(
            self.lat_GS,
            self.lon_GS,
            self.f,
            self.elevation,
            float(self.p),
            self.D_sat,
            hs=None,
            eta=self.etaEarth,
            return_contributions=True,
        )
        self.LA = a_t.value

    def calculate_total_loss(self):
        self.calculate_free_space_attenuation()
        self.calculate_antenna_depointing_loss()
        self.calculate_equipment_loss()
        self.calculate_polarisation_mismatch_loss()
        self.calculate_atmospheric_attenuation()
        self.L = self.L_fs + self.LR + self.LT + self.LFTX + self.LFRX + self.L_POL + self.LA


if __name__ == "__main__":
    link_budget = LinkBudget()
