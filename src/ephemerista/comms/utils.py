import math

SPEED_OF_LIGHT_VACUUM: float = 2.99792458e8


def wavelength(frequency: float) -> float:
    return SPEED_OF_LIGHT_VACUUM / frequency


def to_db(val: float) -> float:
    return 10 * math.log10(val)
