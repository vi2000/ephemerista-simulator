import math
from typing import Literal, Union

import numpy as np
import pydantic_numpy.typing as pnd
from pydantic import BaseModel, Field

from ephemerista.comms.utils import to_db, wavelength


class ParabolicPattern(BaseModel):
    pattern_type: Literal["parabolic"] = Field(default="parabolic")
    diameter: float = Field(gt=0.0)
    efficiency: float = Field(gt=0.0)

    def area(self) -> float:
        return math.pi * self.diameter**2 / 4

    def beamwidth(self, frequency: float) -> float:
        return 70 * wavelength(frequency) / self.diameter

    def gain(self, frequency: float, _angle: float) -> float:
        area = self.area()
        lamb = wavelength(frequency)
        g = to_db(4 * math.pi * area / lamb**2)
        return g + to_db(self.efficiency)


class CustomPattern(BaseModel):
    pattern_type: Literal["custom"] = Field(default="custom")
    angles: pnd.Np1DArrayFp64
    gains: pnd.Np1DArrayFp64
    symmetrical: bool = Field(default=True)

    def gain(self, _frequency: float, angle: float) -> float:
        # TODO: Handle symmetry and out-of-range angles
        return np.interp(angle, self.angles, self.gains)  # type: ignore


class Antenna(BaseModel):
    pattern: Union[ParabolicPattern, CustomPattern] = Field(discriminator="pattern_type")

    def gain(self, frequency: float, angle: float) -> float:
        return self.pattern.gain(frequency, angle)
