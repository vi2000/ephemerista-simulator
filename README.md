# Ephemerista ☕️ 🚀

[![PyPI - Version](https://img.shields.io/pypi/v/ephemerista.svg)](https://pypi.org/project/ephemerista)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/ephemerista.svg)](https://pypi.org/project/ephemerista)
[![coverage](https://gitlab.com/librespacefoundation/ephemerista/ephemerista-simulator/badges/main/coverage.svg?job=coverage)](https://librespacefoundation.gitlab.io/ephemerista/ephemerista-simulator/coverage)

---

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install ephemerista
```

## Development

Please refer to [CONTRIBUTING.md](https://gitlab.com/librespacefoundation/ossmisi/ossmisi-simulator/-/blob/main/CONTRIBUTING.md).

## License

`ephemerista` is distributed under the terms of the [AGPLv3](https://spdx.org/licenses/AGPL-3.0-or-later.html) license.
