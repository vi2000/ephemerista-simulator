import unittest

from ephemerista.orbit.kepler import KeplerianValidator


class TestKeplerianValidator(unittest.TestCase):
    def test_is_physical(self):
        # Test semi-major axis (a) should be greater than 0
        result = KeplerianValidator.is_physical(a=0.0)
        self.assertEqual(result, (False, "Semi-major axis should be positive. Received: 0.0"))

        # Test eccentricity (e) should be between 0 and 1
        result = KeplerianValidator.is_physical(e=-0.5)
        self.assertEqual(result, (False, "Eccentricity is unphysical. Received: -0.5"))

        # Test inclination (i) should be between 0 and 180 degrees
        result = KeplerianValidator.is_physical(i=190)
        self.assertEqual(
            result, (False, "Inclination should be expressed in degrees, between 0 and 180. Received: 190")
        )

        # Test perigee crossing Earth Radius
        result = KeplerianValidator.is_physical(a=6000, e=0.1)
        self.assertEqual(result, (False, "Perigee crosses Earth Radius!"))

        # Test argument of perigee (aop) should be between 0 and 360 degrees
        result = KeplerianValidator.is_physical(aop=-45)
        self.assertEqual(result, (False, "Argument of Perigee should be expressed in degrees, between 0 and 360."))

        # Test eccentric orbit
        result = KeplerianValidator.is_physical(a=7700, e=0.2, i=30, aop=90)
        self.assertEqual(result, (False, "Perigee crosses Earth Radius!"))

        # Test valid inputs
        result = KeplerianValidator.is_physical(a=7700, e=0.02, i=30, aop=90)
        self.assertEqual(result, (True, "Gucci"))
