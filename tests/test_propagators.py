from pathlib import Path

import numpy as np
import pytest
from astropy import units as u
from hapsira.ephem import Ephem
from pytest import approx

from ephemerista.propagators.spice import SpiceKernelPropagator
from ephemerista.scenarios import GroundStation
from ephemerista.time import Time

test_dir = Path(__file__).parent
kernel_path = test_dir.joinpath("resources/lunar/lunar_transfer_2022.bsp")
SpiceKernelPropagator.load_kernel(kernel_path)


# def test_hapsira():
#     r = np.array([-0.107622532467967e07, -0.676589636432773e07, -0.332308783350379e06])
#     v = np.array([0.935685775154103e04, -0.331234775037644e04, -0.118801577532701e04])

#     ep = ISOEpoch.now()
#     c = Cartesian.from_rv(ep, r, v)
#     p = HapsiraPropagator(initialState=c)
#     p.propagate(c, 3600)


def test_spice():
    start_date = Time.from_iso("TDB", "2022-02-01T00:00:00.000").astropy
    end_date = Time.from_iso("TDB", "2022-03-01T00:00:00.000").astropy
    p = SpiceKernelPropagator(id="-10004001")
    ephem = p.propagate([start_date, end_date])
    assert isinstance(ephem, Ephem)


@pytest.mark.skip
def test_groundstation():
    r1_exp = np.array([-1771.962631551279, 4522.356363399281, 4120.057120495107])
    r2_exp = np.array([69.5984881221417, -4859.680780418203, 4116.330667974488])
    gs = GroundStation(latitude=40.4527, longitude=-4.3676, altitude=0.0)
    epochs = [
        Time.from_iso("TDB", "2022-01-31T23:00:00.000").astropy,
        Time.from_iso("TDB", "2022-03-22T06:22:48.027").astropy,
    ]
    eph = gs.propagate(epochs)
    result = eph.rv()
    r1_act: u.Quantity = result[0][0].value  # type: ignore
    r2_act: u.Quantity = result[0][1].value  # type: ignore
    assert r1_exp[0] == approx(r1_act[0], rel=1e-2)
    assert r1_exp[1] == approx(r1_act[1], rel=1e-2)
    assert r1_exp[2] == approx(r1_act[2], rel=1e-2)
    assert r2_exp[0] == approx(r2_act[0], rel=1e-2)
    assert r2_exp[1] == approx(r2_act[1], rel=1e-2)
    assert r2_exp[2] == approx(r2_act[2], rel=1e-2)
