import numpy as np
from pytest import approx

from ephemerista.comms.antennas import Antenna, CustomPattern, ParabolicPattern
from ephemerista.comms.receiver import Receiver, noise_temperature, system_temperature
from ephemerista.comms.utils import wavelength

frequency = 29e9

parabolic = ParabolicPattern(diameter=0.98, efficiency=0.45)
custom = CustomPattern(angles=np.array([0.0, 1.0, 2.0]), gains=np.array([0.0, 1.0, 2.0]))


def test_wavelength():
    act = wavelength(frequency)
    exp = 0.010337670965517241
    assert act == approx(exp)


def test_parabolic_beamwidth():
    exp = 0.7384050689655173
    act = parabolic.beamwidth(frequency)
    assert act == approx(exp)


def test_parabolic_gain():
    exp = 46.01119000490658
    act = parabolic.gain(frequency, 0.0)
    assert act == approx(exp)


def test_custom_gain():
    act = custom.gain(frequency, 1.5)
    exp = 1.5
    assert act == approx(exp)


def test_receiver():
    antenna = Antenna(pattern=ParabolicPattern(diameter=0.1, efficiency=0.6))
    rx = Receiver(antenna=antenna, lna_gain=20, lna_noise_figure=4, noise_figure=5, loss=3)
    act_noise_temp = noise_temperature(rx.noise_figure)
    exp_noise_temp = 627.0605214
    assert act_noise_temp == approx(exp_noise_temp)
    act_system_temp = system_temperature(rx.t_antenna, rx.loss, rx.noise_figure)
    exp_system_temp = 904.53084061
    assert act_system_temp == approx(exp_system_temp)
