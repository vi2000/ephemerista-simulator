from pathlib import Path

import pytest

from ephemerista.propagators.spice import SpiceKernelPropagator
from ephemerista.scenarios import Scenario

test_dir = Path(__file__).parent
kernel_path = test_dir.joinpath("resources/lunar/lunar_transfer_2022.bsp")
SpiceKernelPropagator.load_kernel(kernel_path)


@pytest.mark.skip
def test_deserialization():
    scn_json = test_dir.joinpath("resources/lunar/scenario.json").resolve().read_text()
    scn = Scenario.model_validate_json(scn_json)
    assert isinstance(scn, Scenario)


@pytest.mark.skip
def test_propagation():
    scn_json = test_dir.joinpath("resources/lunar/scenario.json").resolve().read_text()
    scn = Scenario.model_validate_json(scn_json)
    result = scn.propagate()
    assert "Lunar Transfer" in result
