from pathlib import Path

import pytest

from ephemerista.propagators.spice import SpiceKernelPropagator
from ephemerista.scenarios import Scenario

test_dir = Path(__file__).parent
kernel_path = test_dir.joinpath("resources/lunar/lunar_transfer_2022.bsp")
SpiceKernelPropagator.load_kernel(kernel_path)


@pytest.mark.skip
def test_visibility():
    scn_json = test_dir.joinpath("resources/lunar/scenario.json").resolve().read_text()
    scn = Scenario.model_validate_json(scn_json)
    gs = scn.assets[1]
    assert gs.model.asset_type == "groundstation"
    # result = scn.propagate()
    # start_date = scn.start_date.to_astropy()
    # ele = elevation(start_date, gs.model, result["CEBR"], result["Lunar Transfer"])
    # print(ele)
    # visibility(scn)
