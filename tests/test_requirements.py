import ephemerista  # noqa: F401
import ephemerista.test_helpers as helpers


def test_1_1_1():
    """The simulator shall provide a Python-based API."""
    assert "ephemerista" in globals()


@helpers.not_testable
def test_1_1_2():
    """The simulator shall reuse existing OSS astrodynamics libraries that provide a Python-based API."""
    pass


@helpers.not_implemented
def test_1_1_3():
    """The simulator shall allow the addition of new components and systems."""
    raise AssertionError()


@helpers.not_implemented
def test_1_1_4():
    """The simulator shall be licensed under the AGPLv3 licence."""
    raise AssertionError()


@helpers.not_implemented
def test_1_2_1():
    """The simulator shall be developed in the open on a public code forge."""
    raise AssertionError()


@helpers.not_implemented
def test_1_2_2():
    """The simulator source code repository shall be mirrored to ESA code forges (ESA GitLab, Space Codev)."""
    raise AssertionError()


@helpers.not_implemented
def test_1_2_3():
    """The simulator source code shall be covered by an automated unit and integration test suite."""
    raise AssertionError()


@helpers.not_implemented
def test_1_2_4():
    """The simulator test coverage should exceed 90% of the SLOC covered."""
    raise AssertionError()


@helpers.not_implemented
def test_1_2_5():
    """The simulator test suite shall be run for every commit pushed to the source code repository as part of a CI
    pipeline."""
    raise AssertionError()


@helpers.not_implemented
def test_1_2_6():
    """Simulator build artefacts shall be automatically generated for every commit pushed to the source code and
    automatically deployed to an artefact repository."""
    raise AssertionError()


@helpers.not_implemented
def test_1_2_7():
    """SaaS components of the simulator shall be automatically deployed to a staging/testing environment for every
    commit pushed to the source code repository."""
    raise AssertionError()


@helpers.not_implemented
def test_1_3_1():
    """The simulator shall be developed in an iteration-based agile development process that is based on
    ECSS-E-HB-40-01A."""
    raise AssertionError()


@helpers.not_implemented
def test_1_4_1():
    """The simulator shall use the ICRF as the default reference frame."""
    raise AssertionError()


@helpers.not_implemented
def test_1_4_2():
    """The simulator shall provide reference frame transformations from ICRF to body-fixed frames for all celestial
    bodies whose rotational elements are defined by the IAU WGCCRE reports."""
    raise AssertionError()


@helpers.not_implemented
def test_1_4_3():
    """The simulator shall provide reference frame transformations from ICRF to ITRF and all intermediate frames."""
    raise AssertionError()


@helpers.not_implemented
def test_1_4_4():
    """The simulator shall provide reference frame transformations from body-fixed reference frames to topocentric
    frames."""
    raise AssertionError()


@helpers.not_implemented
def test_1_4_5():
    """The simulator shall provide reference frame transformations from ICRF to local and spacecraft-fixed reference
    frames (LVLH, etc.)."""
    raise AssertionError()


@helpers.not_implemented
def test_1_5_1():
    """The simulator shall provide transformations between the following continuous time scales:
    TAI, TT, TDB, TCG, UT1"""
    raise AssertionError()


@helpers.not_implemented
def test_1_5_2():
    """The simulator shall provide dedicated unambiguous transformations between TAI and UTC, taking into account leap
    seconds."""
    raise AssertionError()


@helpers.not_implemented
def test_1_5_3():
    """The simulator shall provide a time scale-aware epoch type."""
    raise AssertionError()


@helpers.not_implemented
def test_1_5_4():
    """The simulator shall provide a duration type."""
    raise AssertionError()


@helpers.not_implemented
def test_1_5_5():
    """The simulator shall support epoch-epoch and epoch-duration arithmetic."""
    raise AssertionError()


@helpers.not_implemented
def test_1_5_6():
    """The simulator shall support the following input and output formats for epochs:
    - Gregorian calendar date and time
    - Gregorian calendar day of year and time
    - Julian day number
    - J2000 Julian day number
    - Two-part Julian day numbers"""
    raise AssertionError()


@helpers.not_implemented
def test_1_6_1():
    """The simulator shall support importing and exporting tabular data in CSV format."""
    raise AssertionError()


@helpers.not_implemented
def test_1_6_2():
    """The simulator should support other formats for tabular data, such as Apache Parquet or Apache Arrow."""
    raise AssertionError()


@helpers.not_implemented
def test_1_6_3():
    """The simulator shall support importing CCSDS Orbit and Navigation Data Messages
    (CCSDS 502.0-B-2, CCSDS 505.0-B-2)."""
    raise AssertionError()


@helpers.not_implemented
def test_1_6_4():
    """The simulator shall support exporting CCSDS Orbit and Navigation Data Messages
    (CCSDS 502.0-B-2, CCSDS 505.0-B-2)."""
    raise AssertionError()


@helpers.not_implemented
def test_1_6_5():
    """The simulator shall be able to read and evaluate planetary and spacecraft ephemerides in binary SPICE kernel
    format (SPK)."""
    raise AssertionError()


@helpers.not_implemented
def test_1_6_6():
    """The simulator shall support importing and exporting hierarchical data in JSON format."""
    raise AssertionError()


@helpers.not_implemented
def test_1_7_1():
    """The public Python API of the simulator shall be fully documented."""
    raise AssertionError()


@helpers.not_implemented
def test_1_7_2():
    """Annotated code examples and tutorials shall be provided for the most common usage scenarios."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_1():
    """The simulator shall provide semi-analytical propagators that consider the gravitational attraction of the
    central body as a point mass and with J2 and J4 coefficients."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_2():
    """The simulator shall provide an SGP4 propagator."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_3():
    """The simulator shall provide a numerical propagator with configurable force model based on a higher-order
    ODE solver."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_4():
    """The numerical propagators shall be able to consider the gravitational attraction of the central body as a point
    mass with J2 and J4 coefficients."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_5():
    """The numerical propagator can load gravity model coefficients from input files."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_6():
    """The numerical propagator shall be able to consider atmospheric drag."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_7():
    """The numerical propagator shall be able to consider solar radiation pressure."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_8():
    """The numerical propagator shall be able to consider third-body perturbations."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_9():
    """The numerical propagator should be able to consider continuous thrust."""
    raise AssertionError()


@helpers.not_implemented
def test_1_8_10():
    """The propagators shall be configurable with time-based and event-based stopping conditions."""
    raise AssertionError()


@helpers.not_implemented
def test_1_9_1():
    """The simulator can detect user-defined events during propagation and precalculated trajectories."""
    raise AssertionError()


@helpers.not_implemented
def test_1_9_2():
    """The simulator shall provide predefined events for periapsis and apoapsis passes."""
    raise AssertionError()


@helpers.not_implemented
def test_1_9_3():
    """The simulator shall provide predefined events for entering and exiting celestial body shadows."""
    raise AssertionError()


@helpers.not_implemented
def test_1_9_4():
    """The simulator shall provide predefined events for detecting unobstructed lines of sight between two objects."""
    raise AssertionError()


@helpers.not_implemented
def test_1_9_5():
    """The simulator shall provide predefined events for an object rising above an elevation threshold for a certain
    location."""
    raise AssertionError()


@helpers.not_implemented
def test_1_9_6():
    """The simulator shall provide predefined events for entering and exiting a celestial body's sphere of influence."""
    raise AssertionError()


@helpers.not_implemented
def test_1_10_1():
    """The simulator shall allow the definition of orbits as Cartesian state vectors."""
    raise AssertionError()


@helpers.not_implemented
def test_1_10_2():
    """The simulator shall allow the definition of orbits as osculating Keplerian elements."""
    raise AssertionError()


@helpers.not_implemented
def test_1_10_3():
    """The simulator shall allow the definition of Keplerian elements in terms of true anomaly or mean anomaly."""
    raise AssertionError()


@helpers.not_implemented
def test_1_10_4():
    """The simulator shall allow the definition of Keplerian elements in terms of semi-major axis or altitude or radius
    pairs."""
    raise AssertionError()


@helpers.not_implemented
def test_1_10_5():
    """The simulator shall allow the definition of trajectories as vectors of epochs and associated state vectors."""
    raise AssertionError()


@helpers.not_implemented
def test_1_10_6():
    """The simulator shall be able to interpolate intermediate state vectors of trajectories."""
    raise AssertionError()


@helpers.not_implemented
def test_1_10_7():
    """The simulator shall be able to change the central body and/or reference frame of an orbit definition or
    trajectory and automatically transform the coordinates."""
    raise AssertionError()


@helpers.not_implemented
def test_1_11_1():
    """The simulator shall provide physical constants for all celestial bodies defined by the IAU WGCCRE reports."""
    raise AssertionError()


@helpers.not_implemented
def test_1_12_1():
    """The simulator can generate ground track plots for trajectories."""
    raise AssertionError()


@helpers.not_implemented
def test_1_12_2():
    """The simulator can generate 3D plots for one or more trajectories."""
    raise AssertionError()


@helpers.not_implemented
def test_1_12_3():
    """The simulator shall be able to plot areas of coverage."""
    raise AssertionError()


@helpers.not_implemented
def test_1_12_4():
    """The simulator shall be able to plot 3D visibility cones."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_1():
    """Scenarios shall require a start and stop epoch."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_2():
    """Scenarios shall require a global default propagator."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_3():
    """Scenarios shall require a global default central body."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_4():
    """Scenarios shall require a global default propagation frame."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_5():
    """It shall be possible to define initial and intermediate orbits for all assets within a scenario."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_6():
    """It shall be possible to modify the state vector of intermediate orbits with impulsive manoeuvres."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_7():
    """It shall be possible to change propagators and propagation parameters between orbits."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_8():
    """It shall be possible to define satellite assets within scenarios."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_9():
    """It shall be possible to define ground assets within scenarios."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_10():
    """It shall be possible to define payloads within scenarios."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_11():
    """It shall be possible to define elevation masks for ground assets."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_12():
    """It shall be possible to define areas or points of interest within scenarios."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_13():
    """It shall be possible to link payloads to assets within scenarios."""
    raise AssertionError()


@helpers.not_implemented
def test_1_13_14():
    """It should be possible to define continuous (low) thrust arcs between orbits."""
    raise AssertionError()


@helpers.not_implemented
def test_1_14_1():
    """The simulator shall provide a GUI that supports the configuration of assets."""
    raise AssertionError()


@helpers.not_implemented
def test_1_14_2():
    """The simulator shall provide a GUI that supports the configuration of analyses."""
    raise AssertionError()


@helpers.not_implemented
def test_1_14_3():
    """The simulator shall provide a GUI that supports the visualisation of propagation, optimisation, and analysis
    results."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_1():
    """Communications payload objects shall consist of antenna, transmitter, and receiver models."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_2():
    """The simulator shall model simple antennas defined by conic angles and pointing mode."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_3():
    """The simulator shall model complex antennas defined by radiation pattern, pointing mode, centre frequency, gain,
    efficiency, and gain-to-noise temperature (G/T)."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_4():
    """The simulator shall model parabolic antennas radiation patterns."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_5():
    """The simulator shall model helix antennas radiation patterns."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_6():
    """The simulator shall model dipole antennas radiation patterns."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_7():
    """The simulator shall model phased array antennas radiation patterns."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_8():
    """The simulator shall support custom file-based antenna radiation patterns."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_9():
    """The simulator shall model nadir antenna pointing for simple antennas."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_10():
    """The simulator shall model zenith antenna pointing for simple antennas."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_11():
    """The simulator shall model targeted antenna pointing for simple antennas."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_12():
    """The simulator shall model nadir antenna pointing for complex antennas."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_13():
    """The simulator shall model zenith antenna pointing for complex antennas."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_14():
    """The simulator shall model targeted antenna pointing for complex antennas."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_15():
    """The simulator shall be able to plot beam contours for any antenna object in both 2D and 3D representations."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_16():
    """The simulator shall require only the necessary parameters for conducting geometrical analyses
    (visibility and access) without the necessity to specify all parameters of the communications payload."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_17():
    """Transmitters shall be defined by central frequency, transmitter gain, and transmitter losses."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_18():
    """Receivers shall be defined by central frequency, low-noise amplifier (LNA) gain, LNA noise,
    and receiver losses."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_19():
    """Payloads shall be connected via communication chains that are modelled as communications channel objects."""
    raise AssertionError()


@helpers.not_implemented
def test_1_15_20():
    """Communications channels shall be defined by data rate, required figure of merit (Eb/N0 or C/N0),
    modulation scheme, roll-off factor, forward error correction, and margins."""
    raise AssertionError()


@helpers.not_implemented
def test_1_16_1():
    """The simulator shall be able to create Walker Star constellations."""
    raise AssertionError()


@helpers.not_implemented
def test_1_16_2():
    """The simulator shall be able to create Walker Delta constellations."""
    raise AssertionError()


@helpers.not_implemented
def test_1_16_3():
    """The simulator shall be able to create Flower constellations."""
    raise AssertionError()


@helpers.not_implemented
def test_1_16_4():
    """The simulator shall be able to create Street of Coverage constellations."""
    raise AssertionError()


@helpers.not_implemented
def test_1_17_1():
    """The simulator shall be able to compute visiblity windows (accesses) for all asset pairings."""
    raise AssertionError()


@helpers.not_implemented
def test_1_17_2():
    """Visibility results shall include observer and target, start and stop epochs and duration of individual windows
    as well as the total duration of all windows."""
    raise AssertionError()


@helpers.not_implemented
def test_1_17_3():
    """Visibility results shall include azimuth, elevation, and range for all windows."""
    raise AssertionError()


@helpers.not_implemented
def test_1_18_1():
    """The simulator shall be able to compute the coverage of a target area on a celestial body which shall be defined
    by polygons or a grid for a given time window."""
    raise AssertionError()


@helpers.not_implemented
def test_1_18_2():
    """Coverage results shall include the percentage of time that the target area is covered for the given
    time window."""
    raise AssertionError()


@helpers.not_implemented
def test_1_18_3():
    """The simulator shall support plotting the coverage results with customisable colours and contours."""
    raise AssertionError()


@helpers.not_implemented
def test_1_19_1():
    """The simulator shall be able to compute link budgets for all visibility windows (accesses) between all assets in
    a given time window."""
    raise AssertionError()


@helpers.not_implemented
def test_1_19_2():
    """Link budget results shall include free space path losses, effective isotropic radiated power (EIRP), G/T, figure
    of merit (Eb/N0 or C/N0), data rates, channel bandwidth, margins, and additional losses."""
    raise AssertionError()


@helpers.not_implemented
def test_1_19_3():
    """Link budget results should include additional losses broken down into output back off (power amplifier), antenna
    pointing loss, depolarization loss, demodulator loss, Solar scintillation effect, implementation losses, ionospheric
    loss, atmospheric loss."""
    raise AssertionError()


@helpers.not_implemented
def test_1_19_4():
    """Link budget results shall be plottable."""
    raise AssertionError()


@helpers.not_implemented
def test_1_20_1():
    """The simulator shall be able to compute time gap analysis and revisit times."""
    raise AssertionError()


@helpers.not_implemented
def test_1_21_1():
    """All inputs and outputs of the simulator shall be provided in machine-readable and serialisable formats to allow
    integration with external optimisation tools."""
    raise AssertionError()


@helpers.not_implemented
def test_1_21_2():
    """The simulator should be optimised for computational performance."""
    raise AssertionError()


@helpers.not_implemented
def test_1_22_1():
    """The simulator shall be able to perform a preliminary assessment of interference analysis for uplink
    and downlink."""
    raise AssertionError()


@helpers.not_implemented
def test_1_23_1():
    """The simulator shall be able to compute for a given satellite network as seen by one or multiple ground assets,
    the following Navigation Performance Figures of Merit:
    - Dilution of Precision (e.g. GDOP, HDOP, etc.)
    - Navigation Accuracy (e.g. HACC, PACC, etc.) (optional)
    - Number of visible satellites.
    - Availability of position fix. (optional)
    - Availability of position accuracy. (optional)
    - Availability of N satellites.
    - Depth of coverage."""
    raise AssertionError()
